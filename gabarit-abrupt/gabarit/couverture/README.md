# Mémo pour produire les éléments graphiques

Pour produire les différents éléments graphiques de cet antilivre, utilisation des commandes suivantes :

- Pour créer la couverture en ascii-art, utilisation de [ascii-image-converter](https://github.com/TheZoraiz/ascii-image-converter) avec la commande : `ascii-image-converter original-contraste.jpg -m ".rtfnex" --save-txt . --font /usr/share/fonts/iosevka/ttf/iosevka-fixed-regular.ttf --font-color 0,0,0 --save-bg 255,255,255 -d 100,50`.
- Pour créer la quatrième, utilisation de [cfonts](https://github.com/dominikwilkowski/cfonts) avec la commande : `cfonts "attendre|l'apocalypse|avec gourmandise" -m 20 -a right > propagande.txt`
- Pour le GIF, utilisation de [ascii-image-converter](https://github.com/TheZoraiz/ascii-image-converter) avec la commande : `ascii-image-converter glitch.gif --save-gif . --font /usr/share/fonts/iosevka/ttf/iosevka-fixed-regular.ttf --font-color 0,0,0 --save-bg 255,255,255 --complex -n
`.

On passe ensuite les fichiers bruts obtenus dans Gimp, avec la police choisie et un DPI de 1200 pour une couverture de 10.8x17.5cm.

La police utilisée est [Iosevka](https://github.com/be5invis/Iosevka). 

