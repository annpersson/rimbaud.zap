--
-- Code from Chickenize TeX package
-- Inspired by ShreevatsaR's answer
-- Source code : https://tex.stackexchange.com/a/401315
--
local interletter_glue   = node.new('glue')
interletter_glue.width   = tex.sp(0)
interletter_glue.stretch = tex.sp('0.01ex') -- This number controls how much the space between letters can stretch.
interletter_glue.shrink = tex.sp('0.01ex') -- This number controls how much the space between letters can shrink.
local interletter_pen    = node.new('penalty')
interletter_pen.penalty  = 10000

add_interletter_glue = function(head)
   -- Adds equivalent of "\penalty10000\hskip 0pt plus 0.5pt" (the above penalty and glue)
   -- before every letter (glyph) that follows a glyph, discretionary (hyphen), or kern node.
   for glyph in node.traverse_id(node.id('glyph'), head) do
      if glyph.prev and (glyph.prev.id == node.id('glyph') or
                         glyph.prev.id == node.id('disc') or
                         glyph.prev.id == node.id('kern')) then
         local g = node.copy(interletter_glue)
         node.insert_before(head, glyph, g)
         node.insert_before(head, g, node.copy(interletter_pen))
      end
   end
   return head
end
luatexbase.add_to_callback("pre_linebreak_filter", add_interletter_glue, "Allow variable interletter spacing.")
