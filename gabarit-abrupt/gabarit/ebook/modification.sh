#!/bin/bash

mkdir tmp-epub

NOM=enfer

unzip ${NOM}.epub -d tmp-epub

for f in $(find tmp-epub/ -name '*.xhtml')
do
  sed -i.bak 's/↩︎/\&nbsp;↑/g' ${f}
  rm ${f}.bak
done

cd tmp-epub/

zip -X0 ${NOM}.epub mimetype
zip -rDX9 ${NOM}.epub * -x "*.DS_Store" -x mimetype

cp ${NOM}.epub ../${NOM}.epub

cd ..

rm -rf tmp-epub
