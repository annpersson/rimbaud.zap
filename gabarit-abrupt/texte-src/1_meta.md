---
author: rimbaud.zap
date: décembre 2021
depot: Premier trimestre 2022
description: Nous installons une ZAP (Zone Autonome à Poétiser) dans le monument Rimbaud et nous y lançons une réécriture frénétique et collective de sa Saison en enfer. Tout y sera amour et irrévérence.
details_colophon: |
  010010010110110000100000011001100110000101110101011101000010000011000011101010100111010001110010011001010010000001100001011000100111001101101111011011000111010101101101011001010110111001110100001000000110110101101111011001000110010101110010011011100110010100101110
identifier:
- scheme: ISBN-13
  text: 978-3-0361-0165-1
imprimerie: Imprimé en Allemagne
informations_generales: |
  La continuité de cet ouvrage se fabrique sur le réseau.

  https://abrupt.cc/zap/rimbaud

  La matière papier résonne en l'antimatière numérique, l'information identique se multiplie, elle découvre sa gratuité, et ce livre trouve son écho en son antilivre.

  https://antilivre.org

  Le mot se disperse dans l'obscur, et il ne nous reste plus qu'à jeter des livres au monde pour manifester rêves et hurlements.

  https://abrupt.cc/manifeste
lang: fr
licence: Cet ouvrage est dédié au domaine public. Il est mis à disposition selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel).
lien_colophon: "https://abrupt.cc/colophon"
lien_licence: "https://abrupt.cc/partage"
lieu: Abrüpt, Internets & Zürich
miroir: La Poésie ne rythmera plus l'action, elle sera en avant
publisher: abrüpt
rights: ∅ 2022 Abrüpt, CC Zero
subject: ZAP, littérature, hack, Rimbaud, Git, poésie
title: enfer.txt
version: 1.0
year: 2022
---

